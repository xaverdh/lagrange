{ pkgs ? import <nixpkgs> {} }:
let
  src = ./. ;
  outName = "lagrange";

  tex = pkgs.texlive.combine ( {
      inherit (pkgs.texlive) scheme-basic collection-luatex fontspec;
    } 
    // texPackages
  );

  texPackages = {
    inherit (pkgs.texlive)
      latexmk
      xcolor
      # scalerel
      mathtools etoolbox
      # faktor
      ;
  };
in pkgs.runCommand "${outName}" {
     buildInputs = [ tex ];
   } ''
    mkdir $out
    export TEXMFVAR="$NIX_BUILD_TOP"
    cd ${src}
    latexmk -lualatex -output-directory="$NIX_BUILD_TOP" -aux-directory="$NIX_BUILD_TOP" -jobname=result main.tex
    cp "$NIX_BUILD_TOP"/result.pdf $out/${outName}.pdf
  ''
