\documentclass{beamer}
\usetheme{Madrid}
\usecolortheme{dolphin}

\setlength{\parindent}{0pt}

% \usepackage[colorlinks,linkcolor=blue]{hyperref}
% \usepackage{xcolor}

\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{scalerel}
\usepackage{tikz-cd}

\newtheorem{thm}{Theorem}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{prop}{Proposition}
\newtheorem*{prop*}{Proposition}
\newtheorem{defn}{Definition}

\newcommand{\dual}[1]{{#1}^{v}}


\title[Lagrangian Mechanics]{Lagrangian Mechanics}

\author{Dominik Xaver Hörl}

\begin{document}

\begin{frame}
\titlepage % Print the title page as the first slide
\end{frame}

\begin{frame}{Overview}
\tableofcontents
\end{frame}

\begin{frame}
  \frametitle{Introduction}
  Lagrangian Mechanics
  \begin{itemize}
    \item Formulates mechanics on the tangent bundle $TQ$ of a configuration space $Q$, instead of its cotangent bundle.
    \item Usually one can pass between the two formulations using a process called "Legendre Transform".
    \item The associated differential equations can be written down in charts, but not in an obviously invariant manner.
      It does admit an action formulation though (which is invariant).
    \item Straightforward formulation when applying the theory to the relativistic realm, since time is "internal"
      (what about quantum mechanics?).
    \item There is a straightforward way to restrict systems:
      If $W \rightarrow Q$ then one has a map $TW \rightarrow TQ$.
  \end{itemize}
\end{frame}

\section{Lagrangian Action}

\begin{frame}
  \frametitle{Lagrangian Action Functional}
  Start with a configuration space $Q$, and a smooth map $TQ \overset{L}{\rightarrow} \mathbb{R}$.
  Now consider this functional on the paths with fixed endpoints $q_1,q_2 \in Q$
  \begin{align*}
    A_L &\colon \left.C^{\infty}\right._{q_1}^{q_2}(I,Q) \rightarrow \mathbb{R} \\
    &\gamma \mapsto  \int_{I} L \circ \dot{\gamma}
  \end{align*}
  where $\dot{\gamma}$ denotes the derivative / tangent lift of $\gamma$, i.e. $I \hookrightarrow I \times \mathbb{R} \overset{\sim}{=} TI \rightarrow TQ, t \mapsto T\gamma(t,1)$.
  
  \vspace{1cm}
  This is called the (Lagrangian) action functional.
  Its critical points will be the solutions (physical trajectories) of our system.

\end{frame}

\begin{frame}
  \frametitle{Critical Points of the Action}
  Given a chart $q \colon (V \subset Q) \overset{\sim}{\rightarrow} U$ we have an induced chart $Tq \colon (TV \subset TQ) \rightarrow TU \overset{\sim}{=} U \times \mathbb{R}^n$ on $TQ$, whose components we denote $q \colon TV \rightarrow U$ and $\dot{q} \colon TV \rightarrow \mathbb{R}^n$.

  For $f \colon TQ \rightarrow \mathbb{R}$ and $v \in TV \subset TQ$ denote by $\partial_{q} f_v$
  and $\partial_{\dot{q}} f_v$ the derivatives $d_{1} (f \circ Tq)_{Tq^{-1}(v)}$ and $d_{2} (f \circ Tq)_{Tq^{-1}(v)}$ wrt. the two factors of $U \times \mathbb{R}^n$. So $\partial_{q} f$ and $\partial_{\dot{q}} f$ are both maps $TV \rightarrow \operatorname{Hom}(\mathbb{R}^n,\mathbb{R}) = (\mathbb{R}^{n})^{*}$.

  \vspace{2mm}
  Then a curve $\gamma \colon I \rightarrow Q$ with tangent lift $\dot{\gamma} \colon I \rightarrow TQ$ is a critical point of $A_L$ precisely if it satisfies the
  \begin{block}{Euler-Lagrange Equations}
  \vspace{-4mm}
    \begin{align*}
      \frac{d}{dt} ({\partial_{\dot{q}} L}_{\dot{\gamma}(t)}) = {\partial_{q} L}_{\dot{\gamma}(t)}
    \end{align*}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Euler-Lagrange Equations}

  For $\gamma \in \left.C^{\infty}\right._{q_1}^{q_2}(I,Q)$ a curve and $\sigma \in T_{\gamma}\left.C^{\infty}\right._{q_1}^{q_2}(I,Q)$ a tangent vector viewed as a map $\sigma \colon \mathbb{R} \times I \rightarrow Q$ with derivative $x = \left.\frac{d}{dt}\right|_{t=0} \sigma_t \in \mathfrak{X}(\gamma)$ and image in a chart $TV \subset TQ$, then:

  \begin{align*}
    \left.dA_L\right|_{\gamma}(\sigma) &=  \left.\frac{d}{ds}\right|_{s=0} \int_{I} L \circ \dot{\sigma_s} \\
    &= \int_{I} \left.\frac{d}{ds}\right|_{s=0} (L \circ Tq^{-1}) \circ (Tq \circ \dot{\sigma_s}) \\
    &= \int_{I} \left.\frac{d}{ds}\right|_{s=0} (L \circ Tq^{-1}) \circ (q \circ \sigma_s,\partial_t (q \circ \sigma_s)) \\
    &= \int_{I} {\partial_{q}L}_{\gamma(t)} \cdot \partial_s (q \circ \sigma_s) + {\partial_{\dot{q}}L}_{\gamma(t)} \cdot \partial_s \partial_t (q \circ \sigma_s) ~dt \\
    &= \int_{I} {\partial_{q}L}_{\gamma(t)} \cdot \partial_s (q \circ \sigma_s) - \left(\frac{d}{dt}{\partial_{\dot{q}}L}_{\gamma(t)} \right) \cdot \partial_s (q \circ \sigma_s) ~dt \\
    &= \int_{I} \left( {\partial_{q}L}_{\gamma(t)} - \frac{d}{dt}{\partial_{\dot{q}}L}_{\gamma(t)} \right) \cdot \partial_s (q \circ \sigma_s) ~dt
  \end{align*}
\end{frame}

\section{Euler-Lagrange Equations}
\begin{frame}
  \frametitle{Euler-Lagrange Equations}
  Varying $\sigma_s$ and thereby $\partial_s (q \circ \sigma_s)$ yields the Euler-Lagrange equations.
  If the curve is not covered by a single chart, we must cover $TQ$ with charts and analyse boundary terms.
\end{frame}

\begin{frame}
  \frametitle{Example: The Free particle}
  Denote by $g$ the pseudoriemannian metric on your spacetime manifold $M$, and consider $L \colon TM \rightarrow \mathbb{R}$, $L(v) = g(v,v)$.

  Using the Euler-Lagrange equations, $\gamma$ will be a critical point of $L$ precisely if $\frac{d}{dt} \dot{\gamma} = 0$ (wrt. the Levi-Civita connection on $M$), i.e. if $\gamma$ is a geodesic.

  \vspace{2mm}
  In the Newtonian limit on $\mathbb{R}^3$ the free particle Lagrangian will look like $L(v) = \frac{m}{2} \|v\|^2$.
\end{frame}

\begin{frame}
  \frametitle{Example: The Pendulum (2D)}

  This is an example of a "classic" mechanical Lagrangian on $T\mathbb{R}^2 \overset{\sim}{=} \mathbb{R}^n \times \mathbb{R}^n$ of the form $L(x,v) = \frac{m}{2} \|v\|^2 - V(x)$ for some potential $V \colon \mathbb{R}^n \rightarrow \mathbb{R}$.

  Can be viewed as coming from the Lagrangian on $\mathbb{R}^2$ with the restriction that it must "live" on a circle $S^1 \overset{i_r}{\hookrightarrow} R^2,~ q \mapsto r \cdot q$ of radius $r$.

  Pull $L$ back by $Ti_r$ to obtain in a chart $(0,2 \pi) \overset{\psi}{\rightarrow} S^1,~ \theta \mapsto (\operatorname{sin}(\theta), \operatorname{cos}(\theta))$
  \begin{align*}
    \widetilde{L}(\theta,\dot{\theta}) = \frac{m}{2} \cdot r^2 \dot{\theta}^2 - (V \circ \psi)(\theta)
  \end{align*}
  The Euler-Lagrange equations then read (along a curve)
  \begin{align*}
    \frac{d}{dt} m r \dot{\theta} = \partial_{\theta} (V \circ \psi)(\theta).
  \end{align*}

  Usually one would have something like $(V \circ \psi)(\theta) = G M m \cdot r (1 - \operatorname{sin}(\theta))$

  as an affine linear approximation of the gravitational field.

\end{frame}

\begin{frame}
  \frametitle{Fiberwise Derivative}
  Let $E \overset{\pi}{\rightarrow} Q$ be a vector bundle.
  Fix some $v \in E$ over $q \coloneqq \pi(v)$, then we have a linear map $P_v \colon E_q \rightarrow T_{v}E,~ w \mapsto [t \mapsto v + t \cdot w]$ with image given by $\operatorname{ker}(d\pi_{v})$. These fit together to form a bundle map $P \colon \pi^{*}E \rightarrow TE$ which yields an isomorphism onto $\operatorname{ker}(d\pi)$, which is also called the \emph{vertical bundle} of $E$.

  Now consider a function $f \colon E \rightarrow \mathbb{R}$, then by the \emph{fiberwise derivative} of $f$ at $v \in E$ over $q =\pi(v) \in Q$ we mean the composition
  \begin{align*}
    {D_{fib}f}_v \colon E_q \overset{P_v}{\longrightarrow} T_{v}E \overset{df_v}{\longrightarrow} \mathbb{R}.
  \end{align*}

  % Also note that if $E = TQ$, $P$ gives us an endomorphism $T_{v}E \overset{T\pi_v}{\rightarrow} T_{\pi(v)}Q = E_q \overset{P_v}{\rightarrow} T_{v}E$.
\end{frame}

\section{Noethers Theorem}

\begin{frame}
  \frametitle{Noethers Theorem}
  By a symmetry of $L \colon TQ \rightarrow \mathbb{R}$ we mean a family $\phi \colon (-\epsilon,\epsilon) \times Q \rightarrow Q$ that leaves $L$ invariant, i.e. $L \circ T\phi_s = L$ for $s \in (-\epsilon,\epsilon)$.

  Denote by $X \coloneqq \left.\frac{d}{ds}\right|_{s=0} \phi_s$ the vector field along $\phi_0$ which is also called the infinitesimal generator of the flow $\phi$.
  \begin{align*}
    & \mu \colon TQ \rightarrow \mathbb{R} \\
    & v \in T_{q}Q \mapsto {D_{fib}L}_v (X_q)
  \end{align*}
  is a constant of motion, i.e. constant along flow lines.
\end{frame}

\begin{frame}
  \frametitle{Noethers Theorem: Example}
  For $n$ (nonrelativistic) free particles on $Q = \mathbb{R}^3$, $L(v) = \Sigma_{i=0}^{n} \frac{m_i}{2} \|v_i\|^2$ is invariant under translation generated by constant vector field $X_q = X$ for $q \in \mathbb{R}^3$, so the momentum ${D_{fib}L}_{v}(X) = \Sigma_{i=0}^{n} m_i \cdot \left\langle v_i, X \right\rangle$ in direction $X$ is conserved.
\end{frame}
\begin{frame}
  \frametitle{Energy}
  Given $L \colon TQ \rightarrow \mathbb{R}$, we may consider the extended configuration space $M \coloneqq Q \times \mathbb{R}$ and extend $L$ as follows:
  \begin{align*}
    TM \overset{\sim}{=} &TQ \times T\mathbb{R} \rightarrow \mathbb{R},~~ \hat{L}(v,(t,t')) = L(v \cdot t') \cdot (t')^{-1}
  \end{align*}

  \begin{itemize}
    \item This is a partial function (not defined when $t'=0$), but we do not care.
    \item A curve $\gamma \colon I \rightarrow Q$ can be lifted to one in $M = Q \times \mathbb{R}$ as $\widetilde{\gamma}(s) = (\gamma(s),s)$ with derivative $\partial_{s} \widetilde{\gamma}(s) = (\dot{\gamma}(s),1)$ where by $1$ we mean the constant vector field with value $1$ on $\mathbb{R}$.
    %\item $A_{\hat{L}}(\widetilde{\gamma}) = \int_{I} \hat{L}( \partial_{s} \widetilde{\gamma}(s)) ~ds = \int_{I} L(\dot{\gamma}(s)) ~ds = A_L(\gamma)$
    \item If $\rho = (\alpha,\phi) \colon I \rightarrow Q \times \mathbb{R}$ is $C^1$ close to some $\widetilde{\gamma}$ with $\rho(a) = \widetilde{\gamma}(a),~ \rho(b)=\widetilde{\gamma}(b)$,
      then $\phi$ is a diffeomorphism $I \rightarrow I$ and we have $A_{\widetilde{L}}(\rho) = A_L(\alpha \circ \phi^{-1})$. So in particular $A_{\hat{L}}(\widetilde{\gamma}) = A_L(\gamma)$.
    \item Therefore lifts of critical points of $A_{L}$ are also critical points of $A_{\tilde{L}}$.
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Energy}
  This extended Lagrangian is invariant under parametrization shift
  \begin{align*}
    \mathbb{R} \times M \rightarrow M,~ (s,(t,q)) \mapsto (t + s,q)
  \end{align*}
  with infinitesimal generator at $p = (q,t) \in M$ given by
  \begin{align*}
    X_p = (0,1) \in TQ \times T\mathbb{R} \overset{\sim}{=} TM.
  \end{align*}

  So for $w = (v,(t,t')) \in TM$ Noether's theorem gives us that
  \begin{align*}
    & \mu(w) = {D_{fib}\widetilde{L}}_w (X_p) = (d\widetilde{L}_w \circ P_w)(X_p) \\
    & = \left.\frac{d}{d\epsilon}\right|_{\epsilon=0} \widetilde{L}(w + \epsilon \cdot X_p) =  \left.\frac{d}{d\epsilon}\right|_{\epsilon=0} L(v \cdot (t' + \epsilon)) \cdot (t' + \epsilon)^{-1} \\
    &= {D_{fib}L}_{v \cdot t'}(v) \cdot (t')^{-1} - (t')^{-2} L(v \cdot t')
  \end{align*}
  is conserved along flow lines on $TM$, so the energy
  \begin{align*}
    E(v) \coloneqq \mu(v,1) = {D_{fib}L}_{v}(v) - L(v)
  \end{align*}
  is conserved along flow lines of $L$ on $TQ$.
\end{frame}

\begin{frame}
  \frametitle{Energy: Examples}
  For a mechanical Lagrangian $L(x,v) = \frac{m}{2} \|v\|^2 - V(x)$ the energy is
  $E(x,v) = V(x) + \frac{m}{2} \|v\|^2$.

  \vspace{2cm}
  What is the "energy" for the relativistic free particle?
\end{frame}

\begin{frame}
  \frametitle{Tangent Cotangent Bundle Isomorphism}

  For $v \in TQ \overset{\pi}{\rightarrow} Q$ and $TQ \overset{L}{\rightarrow} \mathbb{R}$ the fiberwise derivative ${D_{fib}L}_v = dL_v \circ P_v$ gives a linear map $TQ_{\pi(v)} \rightarrow \mathbb{R}$.
  We can see this as a one form ${D_{fib}L}_v \in \dual{TQ}_{\pi(v)}$ and by varying $v$ we get a bundle map

  \begin{align*}
    \tau_{L} \colon TQ \rightarrow \dual{TQ},~ v \mapsto {D_{fib}L}_v
  \end{align*}

  which we call the \emph{Tangent Cotangent Bundle} map.

  \vspace{1cm}
  When is this a diffeomorphism?

\end{frame}

\section{Tangent Cotangent Bundle Isomorphism}

\begin{frame}
  \frametitle{Tangent Cotangent Bundle Isomorphism}
  Call $L$ of \emph{Tonelli type} if when restricted to every fiber:
  \begin{itemize}
    \item it's second derivative is positive definite
    \item it is supercoersive, i.e. grows superlinearily at infinity
  \end{itemize}
  \begin{lemma}
    If $L$ is fiberwise convex and of Tonelli type, then $\tau_L$ is a diffeomorphism.
  \end{lemma}
\end{frame}

\begin{frame}
  \frametitle{Tangent Cotangent Bundle Isomorphism}

  Using this diffeomorphism one can then transport all structure from $TQ$ to $\dual{TQ}$.

  \begin{itemize}
    \item The energy function on the Lagrangian side, gives the Hamiltonian on the other side.
    \item More specifically ${D_{fib}L}_v(v) = {\tau_L^{*} \lambda}_v(v) = \tau_L(v)(v)$ where $\lambda$ is the tautological one form on $\dual{TQ}$.
    \item Paths in $TQ$ can be identified with paths in $\dual{TQ}$ and under this identification
    \item solutions in the sense of critical points of the Lagrangian action will correspond to critical points of the Symplectic action functional, and therefore solutions of the Hamiltonian system on $\dual{TQ}$.
  \end{itemize}
\end{frame}

%\begin{frame}
%  \frametitle{Legendre Transform}
%\end{frame}

\section{Outlook on Field Theories}

\begin{frame}
  \frametitle{Field Theories}
  Replace curves \emph{into} $Q$ by fields \emph{on} $Q$, more precisely sections in some bundle $E \overset{\pi}{\rightarrow} Q$ over it. Q will then be your spacetime manifold of dimension $n$ (usually 4).

  The Lagrangian $L$ will be an $n$-form on the jet bundle $J^kE$ (or more generally even $J^{\infty}E$)

  and the action will look like
  \begin{align*}
    & \Gamma(E) \longrightarrow \mathbb{R} \\
    (\phi &\colon Q \rightarrow E) \mapsto \int_{Q} (j^k\phi)^{*} L.
  \end{align*}

  The single particle case could be recovered in a distributional sense.
\end{frame}

\begin{frame}
  {\large{Space for Writing 0}}
\end{frame}

\begin{frame}
  {\large{Space for Writing 1}}
\end{frame}

\begin{frame}
  {\large{Space for Writing 2}}
\end{frame}



\end{document}
